# Create main volumes directory
mkdir -p ~/taller-observability/volumes

# volume for grafana
mkdir -p ~/taller-observability/volumes/grafana

# volume for alertmanager
mkdir -p ~/taller-observability/volumes/alertmanager/data

# volume for loki
mkdir -p ~/taller-observability/volumes/loki/data

# volume for prometheus
mkdir -p ~/taller-observability/volumes/prometheus/data

# volume for tempo
mkdir -p ~/taller-observability/volumes/tempo/data


###
# Permisions for loki
useradd -u 10001 loki
chown -R loki:loki ~/taller-observability/volumes/loki

# permisions for prometheus
sudo useradd --no-create-home prometheus 
sudo chown -R prometheus:prometheus prometheus

